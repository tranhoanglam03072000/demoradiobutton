let checkRed = () => {
  event.preventDefault();
  document.getElementById("red").checked = true;
  let active = document.querySelector(".active");
  if (active) {
    active.classList.remove("active");
    document.getElementById("btnRed").classList.add("active");
  } else {
    document.getElementById("btnRed").classList.add("active");
  }
};
let checkBlue = () => {
  event.preventDefault();
  document.getElementById("blue").checked = true;
  let active = document.querySelector(".active");
  if (active) {
    active.classList.remove("active");
    document.getElementById("btnBlue").classList.add("active");
  } else {
    document.getElementById("btnBlue").classList.add("active");
  }
};
